var jwt = require('atlassian-jwt'),
    request = require('request');


var OAuthService = function () {
    var AUTHORIZATION_SERVER_URL = "https://auth.atlassian.io",
        GRANT_TYPE = "urn:ietf:params:oauth:grant-type:jwt-bearer",
        SCOPES = "READ ACT_AS_USER";


    function authenticate(addon, clientKey, userKey, callback) {
        addon.settings.get('clientInfo',clientKey).then(function (data) {
            var now = Math.floor(Date.now() / 1000),
                exp = now + 60;

            console.log("\nBuilding JWT Assertion".bold);

            var jwtClaims = {
                iss: "urn:atlassian:connect:clientid:" + data.oauthClientId,
                sub: "urn:atlassian:connect:userkey:" + userKey,
                tnt: data.baseUrl,
                aud: AUTHORIZATION_SERVER_URL,
                iat: now,
                exp: exp
            };
            console.log("JWTClaims:".yellow, jwtClaims);

            var assertion = jwt.encode(jwtClaims, data.sharedSecret);
            console.log("Assertion:".yellow, assertion);


            var parameters = {
                grant_type: GRANT_TYPE,
                assertion: assertion,
                scope: SCOPES
            };
            console.log("Parameters:".yellow, parameters);


            console.log("\nRequesting access token".bold);
            request.post({
                url: AUTHORIZATION_SERVER_URL + '/oauth2/token',
                form: parameters,
                json: true,
                headers: {
                    "accept": "application/json"
                }
            }, function(err, httpResponse, body) {
                var statusCode = httpResponse.statusCode;
                if (err || statusCode < 200 || statusCode > 299) {
                    var error = err || body;
                    console.log(("ERROR [" + statusCode + "]").red + ": Couldn't get access token from response\n", error);
                    return callback({
                        status: statusCode,
                        message: JSON.stringify(error)
                    })
                } else {
                    console.log("Token type:".yellow, body.token_type);
                    console.log("Access token:".yellow, body.access_token);
                    return callback(undefined);
                }
            });

        });
    }


    this.authenticate = authenticate;
}

module.exports = new OAuthService();