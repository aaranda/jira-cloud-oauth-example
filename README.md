This is an OAuth example for JIRA Cloud. It gives you an entry point on the top bar and you will be redirected to a new page that shows you if the OAuth is working on your JIRA instance. You should configure your database properties on the config.properties. By default, it is using a MySQL connection.